#!/bin/bash
sudo apt install -y autoconf automake build-essential cmake git-core libass-dev libfreetype6-dev  libsdl2-dev libtool libva-dev libvdpau-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texinfo wget zlib1g-dev mercurial libnuma-dev subversion cargo

mkdir -p ~/ffmpeg_sources ~/bin

cd ~/ffmpeg_sources
wget https://www.deb-multimedia.org/pool/main/libi/libilbc-dmo/libilbc-dev_2.0.2-dmo5_amd64.deb
 wget https://www.deb-multimedia.org/pool/main/libi/libilbc-dmo/libilbc2_2.0.2-dmo5_amd64.deb
sudo apt install -y ./libilbc-dev_2.0.2-dmo5_amd64.deb  ./libilbc2_2.0.2-dmo5_amd64.deb 

cd ~/ffmpeg_sources
wget https://www.nasm.us/pub/nasm/releasebuilds/2.14/nasm-2.14.tar.bz2
tar xjvf nasm-2.14.tar.bz2
cd nasm-2.14
./autogen.sh
PATH="$HOME/bin:$PATH" ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
make -j$(nproc)
make install

cd ~/ffmpeg_sources
wget -O yasm-1.3.0.tar.gz https://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
tar xzvf yasm-1.3.0.tar.gz
cd yasm-1.3.0
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C x264 pull 2> /dev/null || git clone --depth 1 https://git.videolan.org/git/x264
cd x264
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --enable-static --enable-pic
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

cd ~/ffmpeg_sources
if cd x265 2> /dev/null; then hg pull && hg update; else hg clone https://bitbucket.org/multicoreware/x265; fi
cd x265/build/linux
PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED=off ../../source
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C libvpx pull 2> /dev/null || git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
cd libvpx
PATH="$HOME/bin:$PATH" ./configure --prefix="$HOME/ffmpeg_build" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C fdk-aac pull 2> /dev/null || git clone --depth 1 https://github.com/mstorsjo/fdk-aac
cd fdk-aac
autoreconf -fiv
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make -j$(nproc)
make install

cd ~/ffmpeg_sources
wget -O lame-3.100.tar.gz https://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz
tar xzvf lame-3.100.tar.gz
cd lame-3.100
PATH="$HOME/bin:$PATH" ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared --enable-nasm
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

cd ~/ffmpeg_sources
git clone https://github.com/xiph/ogg.git
cd ogg
./autogen.sh
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared
make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C vorbis pull 2> /dev/null || git clone --depth 1 https://gitlab.xiph.org/xiph/vorbis.git
cd vorbis
./autogen.sh
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared
make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C opus pull 2> /dev/null || git clone --depth 1 https://gitlab.xiph.org/xiph/opus.git
cd opus
./autogen.sh
./configure --prefix="$HOME/ffmpeg_build" --disable-shared
make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C theora pull 2> /dev/null || git clone --depth 1 https://gitlab.xiph.org/xiph/theora.git
cd theora
./autogen.sh
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared
make -j$(nproc)
make install

# cd ~/ffmpeg_sources
# git clone https://github.com/OpenVisualCloud/SVT-AV1.git
# cd SVT-AV1/Build
#  PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" ../
# make -j$(nproc)
# make install




# cd ~/ffmpeg_sources
# git clone https://github.com/intel/SVT-HEVC
# cd SVT-HEVC
# mkdir build && cd build 
# PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" ../
# make -j$(nproc)
# make install


cd ~/ffmpeg_sources
git -C aom pull 2> /dev/null || git clone --depth 1 https://aomedia.googlesource.com/aom
mkdir aom_build
cd aom_build
PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" -DENABLE_SHARED=off -DENABLE_NASM=on ../aom
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

cd ~/ffmpeg_sources
git -C kvazaar pull 2> /dev/null || git clone --depth 1 https://github.com/ultravideo/kvazaar.git
cd kvazaar
./autogen.sh
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --disable-shared
make -j$(nproc)
make install

#cd ~/ffmpeg_sources
#git -C xavs2 pull 2> /dev/null || git clone --depth 1 https://github.com/pkuvcl/xavs2.git
#cd xavs2/build/linux
#./configure \
#  --prefix="$HOME/ffmpeg_build" \
#  --bindir="$HOME/bin" \
#  --bit-depth='8' \
#  --chroma-format='all' \
#  --enable-pic \
#  --enable-shared
#PATH="$HOME/bin:$PATH" make -j$(nproc)
#make install

#cd ~/ffmpeg_sources
#git -C davs2 pull 2> /dev/null || git clone --depth 1 https://github.com/pkuvcl/davs2.git
#cd davs2/build/linux
#./configure \
#  --prefix="$HOME/ffmpeg_build" \
#  --bindir="$HOME/bin" \
#  --bit-depth='8' \
#  --chroma-format='all' \
#  --enable-pic \
#  --enable-shared
#PATH="$HOME/bin:$PATH" make -j$(nproc)
#make install

cd ~/ffmpeg_sources
svn co https://svn.code.sf.net/p/xavs/code/trunk xavs
cd xavs
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin" \
  --enable-pic
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

cd ~/ffmpeg_sources
wget https://sourceforge.net/projects/opencore-amr/files/opencore-amr/opencore-amr-0.1.5.tar.gz
tar xzvf opencore-amr-0.1.5.tar.gz
cd opencore-amr-0.1.5
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

#cd ~/ffmpeg_sources
#wget https://sourceforge.net/projects/opencore-amr/files/vo-amrwbenc/vo-amrwbenc-0.1.3.tar.gz
#tar xzvf vo-amrwbenc-0.1.3.tar.gz
#cd vo-amrwbenc-0.1.3
#PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
#  --prefix="$HOME/ffmpeg_build" \
#  --bindir="$HOME/bin"
#PATH="$HOME/bin:$PATH" make -j$(nproc)
#make install

cd ~/ffmpeg_sources
wget http://downloads.sourceforge.net/twolame/twolame-0.3.13.tar.gz
tar xzvf twolame-0.3.13.tar.gz
cd twolame-0.3.13
./autogen.sh
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install

#cd ~/ffmpeg_sources
#wget https://hobbes1069.fedorapeople.org/freetel/codec2/codec2-0.8.1.tar.xz
#tar xvf codec2-0.8.1.tar.xz
#cd codec2-0.8.1
#mkdir codec2_build
#cd codec2_build
#PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" ../
#PATH="$HOME/bin:$PATH" make -j$(nproc)
#make install

cd ~/ffmpeg_sources
git -C openh264 pull 2> /dev/null || git clone --depth 1 https://github.com/cisco/openh264.git
cd openh264
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" make -j$(nproc)
make install PREFIX="$HOME/ffmpeg_build"

cd ~/ffmpeg_sources
git clone https://github.com/lu-zero/mfx_dispatch.git
autoreconf -i
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install


cd ~/ffmpeg_sources
wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
tar xjvf ffmpeg-snapshot.tar.bz2
cd ffmpeg
git apply ../SVT-AV1/ffmpeg_plugin/0001-Add-ability-for-ffmpeg-to-run-svt-av1.patch
git apply ../SVT-HEVC/ffmpeg_plugin/0001-lavc-svt_hevc-add-libsvt-hevc-encoder-wrapper.patch
git apply ../SVT-HEVC/ffmpeg_plugin/0002-doc-Add-libsvt_hevc-encoder-docs.patch
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
  --prefix="$HOME/ffmpeg_build" \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$HOME/ffmpeg_build/include" \
  --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
  --extra-libs="-lpthread -lm" \
  --bindir="$HOME/bin" \
  --enable-gpl \
  --enable-libaom \
  --enable-libass \
  --enable-libfdk-aac \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libopus \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 \
  --enable-libxavs \
  --enable-libtheora \
  --enable-libopencore-amrnb \
  --enable-libopencore-amrwb \
  --enable-version3 \
  --enable-libtwolame \
  --enable-libilbc \
  --enable-libkvazaar \
  --enable-nonfree
#  --enable-libsvthevc \
#  --enable-libsvtav1 \
#  --enable-libopenh264 \
#  --enable-libvo-amrwbenc \
#  --enable-libmfx \
#  --enable-libxavs2 \
#  --enable-libdavs2 \
#  --enable-shared \
#  --enable-static \
#  --enable-libcodec2 \
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
hash -r

#Experimental
# cd ~/ffmpeg_sources
# git clone https://github.com/divideon/xvc.git
# cd xvc && mkdir build && cd build
# cmake ..
# make



# cd ~/ffmpeg_sources
# git clone https://github.com/xiph/rav1e.git
# cd rav1e
# ./build.sh

