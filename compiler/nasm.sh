#!/bin/bash

echo -e "${BLUE}-----------NASM-----------${NC}"
cd $SOURCEDIR
if [ ! -f nasm-2.14.tar.bz2 ]; then
echo -e "${BLUE}Downloading NASM${NC}"
wget https://www.nasm.us/pub/nasm/releasebuilds/2.14/nasm-2.14.tar.bz2
echo -e "${GREEN}NASM downloaded${NC}"
fi
if [ ! -d nasm-2.14 ]; then
echo -e "${BLUE}Extracting NASM${NC}"
tar xjvf nasm-2.14.tar.bz2
echo -e "${GREEN}NASM extracted${NC}"
fi
cd nasm-2.14
echo -e "${BLUE}Configuring NASM${NC}"
./autogen.sh
PATH=$BINDIR:$PATH ./configure --prefix=$BUILDDIR --bindir=$BINDIR
echo -e "${BLUE}Building NASM${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing NASM${NC}"
make install
echo -e "${GREEN}NASM Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
