#!/bin/bash

echo -e "${BLUE}-----------Yasm-----------${NC}"
cd $SOURCEDIR
if [ ! -f yasm-1.3.0.tar.gz ]; then
echo -e "${BLUE}Downloading Yasm${NC}"
wget -O yasm-1.3.0.tar.gz https://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
echo -e "${GREEN}Yasm downloaded${NC}"
fi
if [ ! -d yasm-1.3.0 ]; then
echo -e "${BLUE}Extracting Yasm${NC}"
tar xzvf yasm-1.3.0.tar.gz
echo -e "${GREEN}Yasm extracted${NC}"
fi
cd yasm-1.3.0
echo -e "${BLUE}Configuring Yasm${NC}"
./configure --prefix=$BUILDDIR --bindir=$BINDIR
echo -e "${BLUE}Building Yasm${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing Yasm${NC}"
make install
echo -e "${GREEN}Yasm Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
