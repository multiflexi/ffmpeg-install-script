#!/bin/bash
source variables


#####################
#Dependencies
#####################
sudo apt update && sudo apt install -y autoconf automake build-essential cmake git-core libass-dev libfreetype6-dev libsdl2-dev libtool libva-dev libvdpau-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texinfo wget zlib1g-dev mercurial libnuma-dev subversion libtool doxygen libssl-dev

# clang cargo

#####################
#Creating directories
#####################
if [ ! -d $BUILDDIR ]; then
	mkdir -v $HOME/ffmpeg_build
	echo -e "${GREEN}Created directory for building in "$BUILDDIR${NC}
fi
if [ ! -d $SOURCEDIR ]; then
	mkdir -v $HOME/ffmpeg_sources
fi
if [ ! -d $BINDIR ]; then
	mkdir -v $HOME/bin
fi

#####################
#Netwide Assembler
#####################
source $SCRIPTDIR/compiler/nasm.sh

#####################
#Yasm
#####################
source $SCRIPTDIR/compiler/yasm.sh

#########SOUND#######

#####################
#iLBC
#####################
#lib/iblc.sh


#####################
#Fraunhofer FDK AAC
#####################
source $SCRIPTDIR/lib/fdk-aac.sh

#####################
#LAME
#####################
source $SCRIPTDIR/lib/lame.sh

#####################
#TwoLAME
#####################
#lib/twolame.sh

#####################
#OGG
#####################
source $SCRIPTDIR/lib/ogg.sh

#####################
#Opus
#####################
source $SCRIPTDIR/lib/opus.sh

#####################
#Vorbis
#####################
source $SCRIPTDIR/lib/vorbis.sh

#####################
#OpenCORE AMR
#####################
#lib/opencore-amr.sh

#####################
#VisualOn AMR-WB
#####################
#lib/vo-amrwbenc.sh

#####################
#Codec 2
#####################
#lib/codec2.sh

#########VIDEO#######

#####################
#nv-codec-headers
#####################
source $SCRIPTDIR/lib/nv-codec-headers.sh

#####################
#x264
#####################
source $SCRIPTDIR/lib/x264.sh

#####################
#x265
#####################
source $SCRIPTDIR/lib/x265.sh

#####################
#Kvazaar
#####################
source $SCRIPTDIR/lib/kvazaar.sh

####################
#LibVPX
#####################
source $SCRIPTDIR/lib/libvpx.sh

#####################
#AOM
#####################
source $SCRIPTDIR/lib/aom.sh

#####################
# rav1e
#####################
# source $SCRIPTDIR/lib/rav1e.sh

#####################
# dav1d
#####################
# source $SCRIPTDIR/lib/dav1d.sh

#####################
#Theora
#####################
#source $SCRIPTDIR/lib/theora.sh


#####################
#Xvid
#####################
#lib/xvid.sh

#####################
#XAVS
#####################
#source $SCRIPTDIR/lib/xavs.sh

#####################
#XAVS2
#####################
#lib/xavs2.sh

#####################
#OpenH264
#####################
#lib/openh264.sh

#####################
#Intel media sdk dispatcher
#####################
#lib/mfx.sh


#####################
#SoX Resampler
#####################
#source $SCRIPTDIR/lib/soxr.sh



#####################
#FFmpeg
#####################

cd $SOURCEDIR
wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
tar xjvf ffmpeg-snapshot.tar.bz2
cd ffmpeg
PATH=$BINDIR:$PATH PKG_CONFIG_PATH="$BUILDDIR/lib/pkgconfig" ./configure \
  --extra-version=kme$d
  --prefix=$BUILDDIR \
  --pkg-config-flags="--static" \
  --extra-cflags="-I$BUILDDIR/include" \
  --extra-ldflags="-L$BUILDDIR/lib" \
  --extra-libs="-lpthread -lm" \
  --bindir=$BINDIR \
  --enable-gpl \
  --enable-libaom \
  --enable-libdav1d\
  --enable-librav1e\
  --enable-libass \
  --enable-libfreetype \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libmp3lame \
  --enable-libx265 \
  --enable-libkvazaar \
  --enable-libvorbis \
  --enable-libfdk-aac \
  --enable-libopus \
  --enable-cuda\
  --enable-cuvid\
  --enable-nvenc\
  --enable-nonfree\
  --enable-openssl\
  --enable-libnpp\
  --extra-cflags="-I/usr/local/cuda/include" \
  --extra-ldflags="-L/usr/local/cuda/lib64" \
  --enable-nonfree
#  --enable-libsoxr \
#  --enable-libtheora \
#  --enable-version3 \
#  --enable-libxavs \
#  --enable-libopencore-amrnb \
#  --enable-libopencore-amrwb \
#  --enable-libtwolame \
#  --enable-libilbc \
#  --enable-libxvid \
#  --enable-libopenh264 \
#  --enable-libvo-amrwbenc \
#  --enable-libmfx \
#  --enable-libxavs2 \
#  --enable-libdavs2 \
#  --enable-shared \
#  --enable-static \
#  --enable-libcodec2 \
PATH=$BINDIR:$PATH make -j$NJOBS
make install
hash -r
