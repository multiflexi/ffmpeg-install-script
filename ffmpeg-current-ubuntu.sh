#!/bin/bash

sudo apt update && sudo apt -y full-upgrade
sudo apt build-dep ffmpeg

builddir=$HOME/devel/ffmpeg

if [ ! -d $builddir ]; then
	mkdir -p $builddir
	echo -e "${GREEN}Downloaded ffmpeg package source"{NC}
else	rm -rf $builddir && mkdir -p $builddir
	echo -e "${GREEN}Downloaded the current ffmpeg package source"{NC}
fi
cd $builddir
sudo apt source ffmpeg
sudo chown -hR $USER: *
cd ffmpeg-4.1.4
echo -e "${GREEN}Building ffmpeg package"{NC}
debuild -b --no-sign --jobs-try=$((`nproc`))
echo -e "${GREEN}Removing unnecesarry packages"{NC}
cd ..
rm -f libavcodec-extra* libavfilter-extra*
echo -e "${GREEN}Installing ffmpeg package"{NC}
sudo dpkg -i *.deb
echo -e "${GREEN}Marking packages ffmpeg relaed packages to not update with apt"{NC}
sudo apt-mark hold ffmpeg ffmpeg-doc libavcodec-dev libavcodec58 libavfilter7 libavfilter-dev libavformat58 libavformat-dev libavresample4 libavresample-dev libavutil-dev libavutil56 libavdevice58 libavdevice-dev libswscale5 libswscale-dev libswresample3 libswresample-dev libpostproc55 libpostproc-dev
