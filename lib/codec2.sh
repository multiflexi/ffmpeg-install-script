#!/bin/bash

cd ~/ffmpeg_sources
wget https://hobbes1069.fedorapeople.org/freetel/codec2/codec2-0.8.1.tar.xz
tar xvf codec2-0.8.1.tar.xz
cd codec2-0.8.1
mkdir codec2_build
cd codec2_build
PATH="$HOME/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$HOME/ffmpeg_build" ../
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
