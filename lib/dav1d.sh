#!/bin/bash

echo -e "${BLUE}-----------AOM-----------${NC}"
cd $SOURCEDIR
if [ ! -d aom ]; then
echo -e "${BLUE}Cloning AOM repo${NC}"
git clone --depth 1 https://aomedia.googlesource.com/aom
mkdir aom/aom_build && cd aom/aom_build
elif [ ! -d aom/aom_build ]; then
mkdir aom/aom_build && cd aom/aom_build && git pull
else cd aom/aom_build && git pull
fi
echo -e "${BLUE}Configuring AOM${NC}"
PATH=$BINDIR:$PATH cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$BUILDDIR -DENABLE_SHARED=off -DENABLE_NASM=on ../../aom
echo -e "${BLUE}Building AOM${NC}"
PATH=$BINDIR:$PATH make -j3
make install && echo -e "${GREEN}AOM Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
