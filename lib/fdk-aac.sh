#!/bin/bash

echo -e "${BLUE}-----------fdk-aac-----------${NC}"
cd $SOURCEDIR
echo -e "${BLUE}Cloning fdk-aac repo${NC}"
git -C fdk-aac pull 2> /dev/null || git clone --depth 1 https://github.com/mstorsjo/fdk-aac
echo -e "${GREEN}fdk-aac cloned${NC}";
cd fdk-aac
echo -e "${BLUE}Configuring fdk-aac${NC}"
autoreconf -fiv
./configure --prefix=$BUILDDIR --disable-shared
echo -e "${BLUE}Building fdk-aac${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing fdk-aac${NC}"
make install && echo -e "${GREEN}fdk-aac Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
