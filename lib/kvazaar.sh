#!/bin/bash

echo -e "${BLUE}-----------Kvazaar-----------${NC}"
cd $SOURCEDIR
if [ ! -d kvazaar ]; then
echo -e "${BLUE}Cloning Kvazaar repo${NC}"
git clone --depth 1 https://github.com/ultravideo/kvazaar.git
cd kvazaar
echo -e "${GREEN}Kvazaar cloned${NC}";
else cd kvazaar && git pull
fi
echo -e "${BLUE}Configuring Kvazaar${NC}"
./autogen.sh
./configure --prefix=$BUILDDIR --bindir=$BINDIR --disable-shared
echo -e "${BLUE}Building Kvazaar${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing Kvazaar${NC}"
make install && echo -e "${GREEN}Kvazaar Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
