#!/bin/bash

echo -e "${BLUE}-----------LAME-----------${NC}"
cd $SOURCEDIR
echo -e "${BLUE}Cloning LAME repo${NC}"
wget -O lame-3.100.tar.gz https://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz
echo -e "${GREEN}LAME cloned${NC}";
tar xzvf lame-3.100.tar.gz
cd lame-3.100
echo -e "${BLUE}Configuring LAME${NC}"
PATH=$BINDIR:$PATH ./configure --prefix=$BUILDDIR --bindir=$BINDIR --disable-shared --enable-nasm
echo -e "${BLUE}Building LAME${NC}"
PATH=$BINDIR:$PATH make -j$NJOBS
echo -e "${BLUE}Installing LAME${NC}"
make install && echo -e "${GREEN}LAME Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
