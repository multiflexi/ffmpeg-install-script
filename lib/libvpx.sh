#!/bin/bash

echo -e "${BLUE}-----------LibVPX-----------${NC}"
cd $SOURCEDIR
if [ ! -d libvpx ]; then
echo -e "${BLUE}Cloning LibVPX repo${NC}"
git clone --depth 1 https://chromium.googlesource.com/webm/libvpx.git
cd libvpx
else cd $SOURCEDIR/libvpx && git pull
fi
echo -e "${BLUE}Configuring LibVPX${NC}"
PATH=$BINDIR:$PATH ./configure --prefix=$BUILDDIR --disable-examples --disable-unit-tests --enable-vp9-highbitdepth --as=yasm
echo -e "${BLUE}Building LibVPX${NC}"
PATH=$BINDIR:$PATH make -j$NJOBS
echo -e "${BLUE}Installing LibVPX${NC}"
make install && echo -e "${GREEN}LibVPX Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
