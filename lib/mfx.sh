#!/bin/bash

cd ~/ffmpeg_sources
git clone https://github.com/lu-zero/mfx_dispatch.git
autoreconf -i
./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
