#!/bin/bash

echo -e "${BLUE}-----------nv-codec-headers---------${NC}"
cd $SOURCEDIR
if [ ! -d nv-codec-headers ]; then
echo -e "${BLUE}Cloning nv-codec-headers repo${NC}"
git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git
cd nv-codec-headers
echo -e "${GREEN}nv-codec-headers cloned${NC}";
else cd nv-codec-headers && git pull
fi
echo -e "${BLUE}Building nv-codec-headers${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing nv-codec-headers${NC}"
sudo make install && echo -e "${GREEN}nv-codec-headers Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
