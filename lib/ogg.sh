#!/bin/bash


echo -e "${BLUE}-----------OGG-----------${NC}"
cd $SOURCEDIR
if [ ! -d ogg ]; then
echo -e "${BLUE}Cloning OGG repo${NC}"
git clone https://github.com/xiph/ogg.git
cd $SOURCEDIR/ogg
echo -e "${GREEN}OGG cloned${NC}";
else cd $SOURCEDIR/ogg && git pull;
fi
echo -e "${BLUE}Configuring OGG${NC}"
./autogen.sh
./configure --prefix=$BUILDDIR --bindir=$BINDIR --enable-shared
echo -e "${BLUE}Building OGG${NC}"
PATH=$BINDIR:$PATH make -j$NJOBS
echo -e "${BLUE}Installing OGG${NC}"
make install && echo -e "${GREEN}OGG Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
