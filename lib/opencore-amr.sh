#!/bin/bash

cd ~/ffmpeg_sources
wget https://sourceforge.net/projects/opencore-amr/files/opencore-amr/opencore-amr-0.1.5.tar.gz
tar xzvf opencore-amr-0.1.5.tar.gz
cd opencore-amr-0.1.5
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
