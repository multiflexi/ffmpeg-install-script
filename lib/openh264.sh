#!/bin/bash

cd ~/ffmpeg_sources
git -C openh264 pull 2> /dev/null || git clone --depth 1 https://github.com/cisco/openh264.git
cd openh264
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" make -j$(nproc)
make install PREFIX="$HOME/ffmpeg_build"
