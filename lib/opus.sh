#!/bin/bash

echo -e "${BLUE}-----------Opus-----------${NC}"
cd ~/ffmpeg_sources
echo -e "${BLUE}Cloning Opus repo${NC}"
git -C opus pull 2> /dev/null || git clone --depth 1 https://gitlab.xiph.org/xiph/opus.git
echo -e "${GREEN}Opus cloned${NC}";
cd opus
echo -e "${BLUE}Configuring Opus${NC}"
./autogen.sh
./configure --prefix=$BUILDDIR --enable-shared
echo -e "${BLUE}Building Opus${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing Opus${NC}"
make install && echo -e "${GREEN}Opus Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
