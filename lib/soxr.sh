#!/bin/bash

echo -e "${BLUE}-----------SoX-----------${NC}"
cd $SOURCEDIR
echo -e "${BLUE}Cloning SoX repo${NC}"
wget -O soxr-0.1.3-Source.tar.xz https://sourceforge.net/projects/soxr/files/soxr-0.1.3-Source.tar.xz/download
echo -e "${GREEN}SoX cloned${NC}";
tar xvf soxr-0.1.3-Source.tar.xz
cd soxr-0.1.3-Source
mkdir build
cd build
echo -e "${BLUE}Configuring SoX${NC}"
PATH=$BINDIR:$PATH cmake -G "Unix Makefiles" -Wno-dev -DCMAKE_INSTALL_PREFIX=$BUILDDIR -DCMAKE_BUILD_TYPE=Release -DENABLE_SHARED=off ..
echo -e "${BLUE}Building SoX${NC}"
PATH=$BINDIR:$PATH make -j$NJOBS
echo -e "${BLUE}Installing SoX${NC}"
make install && echo -e "${GREEN}SoX Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
