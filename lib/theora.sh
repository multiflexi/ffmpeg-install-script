#!/bin/bash

echo -e "${BLUE}-----------Theora-----------${NC}"
cd $SOURCEDIR
if [ ! -d theora ]; then
echo -e "${BLUE}Cloning Theora repo${NC}"
git clone --depth 1 https://gitlab.xiph.org/xiph/theora.git
cd theora
echo -e "${GREEN}Theora cloned${NC}";
else cd theora && git pull
fi
echo -e "${BLUE}Configuring Theora${NC}"
./autogen.sh
./configure --prefix=$BUILDDIR --bindir=$BINDIR --disable-shared
echo -e "${BLUE}Building Theora${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing Theora${NC}"
make install && echo -e "${GREEN}Theora Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
