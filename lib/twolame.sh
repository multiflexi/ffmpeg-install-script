#!/bin/bash

cd ~/ffmpeg_sources
wget http://downloads.sourceforge.net/twolame/twolame-0.3.13.tar.gz
tar xzvf twolame-0.3.13.tar.gz
cd twolame-0.3.13
./autogen.sh
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
