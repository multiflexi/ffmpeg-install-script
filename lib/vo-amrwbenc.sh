#!/bin/bash

cd ~/ffmpeg_sources
wget https://sourceforge.net/projects/opencore-amr/files/vo-amrwbenc/vo-amrwbenc-0.1.3.tar.gz
tar xzvf vo-amrwbenc-0.1.3.tar.gz
cd vo-amrwbenc-0.1.3
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" ./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin"
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
