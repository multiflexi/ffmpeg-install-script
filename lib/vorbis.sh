#!/bin/bash

echo -e "${BLUE}-----------Vorbis-----------${NC}"
cd $SOURCEDIR
if [ ! -d vorbis ]; then
echo -e "${BLUE}Cloning Vorbis repo${NC}"
git clone --depth 1 https://gitlab.xiph.org/xiph/vorbis.git
cd vorbis
echo -e "${GREEN}Vorbis cloned${NC}";
else cd vorbis && git pull
fi
echo -e "${BLUE}Configuring Vorbis${NC}"
./autogen.sh
./configure --prefix=$BUILDDIR --bindir=$BINDIR
echo -e "${BLUE}Building Vorbis${NC}"
make -j$NJOBS
echo -e "${BLUE}Installing Vorbis${NC}"
make install && echo -e "${GREEN}Vorbis Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
