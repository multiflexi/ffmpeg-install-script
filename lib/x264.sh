#!/bin/bash


echo -e "${BLUE}-----------x264-----------${NC}"
cd $SOURCEDIR
if [ ! -d x264 ]; then
echo -e "${BLUE}Cloning x264 repo${NC}"
git clone --depth 1 https://git.videolan.org/git/x264
cd $SOURCEDIR/x264
echo -e "${GREEN}x264 cloned${NC}";
else cd $SOURCEDIR/x264 && git pull;
fi
echo -e "${BLUE}Configuring x264${NC}"
PATH=$BINDIR:$PATH PKG_CONFIG_PATH="$BUILDDIR/lib/pkgconfig" ./configure --prefix=$BUILDDIR --bindir=$BINDIR --enable-static --enable-pic
echo -e "${BLUE}Building x264${NC}"
PATH=$BINDIR:$PATH make -j$NJOBS
echo -e "${BLUE}Installing x264${NC}"
make install && echo -e "${GREEN}x264 Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
