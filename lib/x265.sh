#!/bin/bash


echo -e "${BLUE}-----------x265-----------${NC}"
cd $SOURCEDIR
if [ ! -d x265 ]; then
echo -e "${BLUE}Cloning x265 repo${NC}"
hg clone https://bitbucket.org/multicoreware/x265
cd $SOURCEDIR/x265/build/linux
echo -e "${GREEN}x265 cloned${NC}";
else cd $SOURCEDIR/x265 && hg pull && hg update
cd build/linux;
fi
echo -e "${BLUE}Configuring x265${NC}"
PATH=$BINDIR:$PATH cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$BUILDDIR -DENABLE_SHARED=off ../../source
echo -e "${BLUE}Building x265${NC}"
PATH=$BINDIR:$PATH make -j$NJOBS
make install && echo -e "${GREEN}x265 Installed${NC}"
read -n 1 -s -r -p "Press any key to continue..."
echo -e ""
