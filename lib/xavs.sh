#!/bin/bash

cd ~/ffmpeg_sources
svn co https://svn.code.sf.net/p/xavs/code/trunk xavs
cd xavs
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin" \
  --enable-pic
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
