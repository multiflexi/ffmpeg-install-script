#!/bin/bash

cd ~/ffmpeg_sources
git -C xavs2 pull 2> /dev/null || git clone --depth 1 https://github.com/pkuvcl/xavs2.git
cd xavs2/build/linux
./configure \
  --prefix="$HOME/ffmpeg_build" \
  --bindir="$HOME/bin" \
  --bit-depth='8' \
  --chroma-format='all' \
  --enable-pic \
  --enable-shared
PATH="$HOME/bin:$PATH" make -j$(nproc)
make install
