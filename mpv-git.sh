#!/bin/bash

sudo apt update && sudo apt -y full-upgrade
sudo apt -y install rst2pdf vulkan-utils

MPV=$HOME/mpv

if [ ! -d $MPV ]; then
	cd $HOME && git clone https://github.com/mpv-player/mpv.git	
	echo -e "${GREEN}Cloned mpv from GitHub"{NC}
else	cd $MPV && git pull
	echo -e "${GREEN}Updated mpv from GitHub"{NC}
fi
cd $MPV
echo -e "${GREEN}Checking for waf"{NC}
./bootstrap.py
echo -e "${GREEN}Configuring mpv"{NC}
./waf configure --enable-html-build #--enable-pdf-build
echo -e "${GREEN}Building mpv"{NC}
./waf
sudo ./waf install
