#!/bin/bash

sudo apt update && sudo apt -y full-upgrade

builddir=$HOME/nv-codec-headers

if [ ! -d $builddir ]; then
	cd $HOME && git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git
	echo -e "${GREEN}Cloned nv-codec-headers from GitHub"{NC}
else	cd $builddir && git pull
	echo -e "${GREEN}Updated nv-codec-headers from GitHub"{NC}
fi
cd $builddir
git clone https://git.videolan.org/git/ffmpeg/nv-codec-headers.git
cd nv-codec-headers
make && sudo make install
