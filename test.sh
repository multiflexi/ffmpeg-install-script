#!/bin/bash

#SVT-AV1

~/bin/ffmpeg -i ~/Downloads/city_4cif.y4m -nostdin -f rawvideo -pix_fmt yuv420p - | ./SvtAv1EncApp -i stdin -n 600 -w 704 -h 576

#xvc

~/bin/ffmpeg -i ~/Downloads/city_4cif.y4m -f yuv4mpegpipe - | ./xvcenc -input-file - -output-file ~/Downloads/city.xvc

#rav1e
~/bin/ffmpeg -i ~/Downloads/city_4cif.y4m -nostdin -f rawvideo -pix_fmt yuv420p - | cargo run --release --bin rav1e -- stdin -o output.ivf
